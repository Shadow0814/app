<?php

Route::get('index', 'pagesController@inicio');

Route::get('galerias','pagesController@galeria')->name('fotos');

Route::get('noticias','pagesController@blog')->name('noticias');

Route::get('nosotros/{nombre?}','pagesController@nosotros')->name('nosotros');