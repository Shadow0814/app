<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class pagesController extends Controller
{
	public function inicio(){
		return view('home');
	}
	public function galeria(){
		return view('fotos');
	}
	public function blog(){
		return view('blog');
	}
	public function nosotros($nombre = null){
		$equipo = ['Juan','Ignacio','Pedro'];
	// return view('nosotros', ['equipo'=>$equipo]);
	return view('nosotros',compact('equipo','nombre'));
	}
}
